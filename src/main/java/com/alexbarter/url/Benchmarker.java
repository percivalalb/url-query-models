/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
//package com.alexbarter.util;
//
//import java.util.Arrays;
//import java.util.List;
//
//public class Benchmarker {
//
//    public static void main(String[] args) {
//        String input = "key=value&dad=&dawewa=2134143123&drdtewsr=123125rfesd";
//        // Warm up
//        int i = 0;
//        for (i = 0; i < 100; i++) {
//            String[] v = input.split("&");
//        }
//
//        long start = System.nanoTime();
//
//        for (i = 0; i < 10000000; i++) {
//            String[] v = input.split("&");
//        }
//
//        long end = System.nanoTime();
//
//        System.out.println(String.format("Time: %dms, Ite: %d", (end - start) / 1000000, i));
//
//
//
//        for (i = 0; i < 100; i++) {
//            List<String> v = Util.splitSingle(input, '&');
//        }
//
//        start = System.nanoTime();
//
//        for (i = 0; i < 10000000; i++) {
//            List<String> v = Util.splitSingle(input, '&');
//        }
//
//        end = System.nanoTime();
//
//        System.out.println(String.format("Time: %dms, Ite: %d", (end - start) / 1000000, i));
//    }
//}
