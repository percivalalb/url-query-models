/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.url.models;

import com.alexbarter.url.models.compile.exception.URLQueryInvalidException;
import com.alexbarter.url.models.definition.IParamParser;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class Util {

    public static IParamParser<?> getTransformer(@Nonnull Class<?> type) {
        if (type == Integer.class || type == Integer.TYPE) {
            return Integer::valueOf;
        } else if (type == Double.class || type == Double.TYPE) {
            return Double::valueOf;
        } else if (type == Float.class || type == Float.TYPE) {
            return Float::valueOf;
        } else if (type == Short.class || type == Short.TYPE) {
            return Short::valueOf;
        } else if (type == Byte.class || type == Byte.TYPE) {
            return Byte::valueOf;
        } else if (type == Long.class || type == Long.TYPE) {
            return Long::valueOf;
        } else if (type == Boolean.class || type == Boolean.TYPE) {
            return Boolean::valueOf;
        } else if (type == Character.class || type == Character.TYPE) {
            return (str) -> {
                if (str.length() > 1) {
                    throw new RuntimeException();
                }

                if (str.isEmpty()) {
                    return null;
                }

                return str.charAt(0);
            };
        } else if (type == LocalDate.class) {
            return LocalDate::parse;
        } else if (type == LocalDateTime.class) {
            return LocalDateTime::parse;
        } else if (type == Date.class) {
            return Date::parse;
        } else if (type == BigDecimal.class) {
            return (str) -> new BigDecimal(str.toCharArray());
        } else if (type == BigInteger.class) {
            return (str) -> new BigInteger(str);
        } else if (type.isEnum()) {
            Enum[] enums = (Enum[]) type.getEnumConstants();
            return (str) -> {
                for (Enum e : enums) {
                    if (e.name().equalsIgnoreCase(str)) {
                        return e;
                    }
                }
                throw new URLQueryInvalidException("No matching enum");
            };
        } else if (type == String.class) {
            return (str) -> str;
        }

        return null;
    }
//
//    // Equivalent of input.split(splitOn, 2)
//    public static List<String> splitSingle(String input, final char splitOn) {
//        int off = 0;
//        int next;
//
//        ArrayList<String> list = new ArrayList<>();
//        while ((next = input.indexOf(splitOn, off)) != -1) {
//
//            // Check the string is
//            if (off != next) {
//                list.add(input.substring(off, next));
//            }
//            off = next + 1;
//        }
//
//        // If no match was found, return this
//        if (off == 0) {
//            return List.of(input);
//        }
//
//        // Add remaining segment
//        if (off < input.length()) {
//            list.add(input.substring(off));
//        }
//
//        return list;
//    }

}
