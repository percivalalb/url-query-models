/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.url.models.compile.exception;

public class Codes {

    // Compile errors
    public static final byte INVALID_ENCLOSING = 1;
    public static final byte INVALID_CONSTRUCTOR = 3;
    public static final byte INVALID_MAP = 4;
    public static final byte MISSING_PARSER = 5;
    public static final byte INVALID_GENERIC = 10;

    // Parse errors
    public static final byte PARAM_REQUIRED = 2;
    public static final byte NON_TERMINATING_BRACKET = 6;
    public static final byte NEGATIVE_INDEX = 7;
    public static final byte NON_NUMERIC_INDEX = 8;
    public static final byte EMPTY_INDEX = 9;
    public static final byte INVALID_SCALAR = 11;
}
