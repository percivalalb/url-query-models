/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.url.models.compile.structure;

public class SetStructure extends Structure {

    private java.lang.reflect.Type componentType;

    public SetStructure(Class listType, java.lang.reflect.Type componentType) {
        super(Type.SET, listType);
        this.componentType = componentType;
    }

    public java.lang.reflect.Type getComponentType() {
        return this.componentType;
    }

    @Override
    public String toString() {
        return "SetStructure{" +
                "type=" + this.getType() +
                ", key=" + this.componentType +
                '}';
    }
}
