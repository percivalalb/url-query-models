/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.url.models.compile;

import com.alexbarter.url.models.compile.exception.Codes;
import com.alexbarter.url.models.compile.exception.UrlQueryException;
import com.alexbarter.url.models.compile.exception.UrlQueryParseException;
import com.alexbarter.url.models.compile.structure.*;
import com.alexbarter.url.models.definition.ParamOptions;

import java.lang.reflect.*;
import java.util.*;

public final class CompiledParam {

    private final Field field;
    private final String name;
    private final List<Structure> structure;
    private final ParamOptions options;

    public CompiledParam(Field field, String name, List<Structure> structure, ParamOptions options) {
        this.field = field;
        this.name = name;
        this.structure = structure;
        this.options = options;
    }

    public Field getField() {
        return this.field;
    }

    public String getName() {
        return this.name;
    }

    public <T> Object parse(Object parent, String[][] keyValues) {
        return parseInternal(0, parent, keyValues, this.name);
    }

    private <T> Object parse(int level, Object parent, String[][] keyValues, String prefix) {
        return parseInternal(level, parent, keyValues, prefix);
    }

    private <T> Object parseInternal(int level, Object parent, String[][] keyValues, String prefix) {
        Structure struc = this.structure.get(level);
        //System.out.println("BASE: " + struc);
        switch (struc.getStructure()) {
            case ARRAY:
                return parseArray(level + 1, parent, keyValues, prefix, struc.getType());
            case LIST:
               return parseList(level + 1, parent, keyValues, prefix, ((ListStructure) struc).getComponentType());
            case SET:
                return parseSet(level + 1, parent, keyValues, prefix, ((SetStructure) struc).getComponentType());
            case MAP:
                return parseMap(level + 1, parent, keyValues, prefix, ((MapStructure) struc).getType());
            case MODEL:
                return ((ModelStructure) struc).getModel().parseForSubModel(parent, keyValues, prefix);
            case SCALAR:
                for (String[] pair : keyValues) {
                    if (pair[0].equals(prefix)) {
                        try {
                            return ((ScalarStructure) struc).getParser().transform(pair[1]);
                        } catch (UrlQueryParseException e) {
                            if (this.options.propagateErrors()) {
                                throw e;
                            }

                            break;
                        }
                    }
                }

                if (this.options.isRequired()) {
                    throw new UrlQueryException(Codes.PARAM_REQUIRED, "Param required at " + prefix);
                }

                break;
        }


        return null;
    }

    public <T> Map parseMap(int level, Object parent, String[][] keyValues, String prefix, Class<T> mapType) {

        Map map = new HashMap<>();
        Structure struc = this.structure.get(level);

        for (String[] pair : keyValues) {
            if (pair[0].startsWith(prefix + "[")) {
                String indexPart = null;

                int j = prefix.length();
                char ch;
                do {
                    if (++j == pair[0].length()) {
                        throw new UrlQueryParseException(Codes.NON_TERMINATING_BRACKET, "No terminating ] found");
                    }

                    ch = pair[0].charAt(j);
                    if (ch == ']') {
                        // If the next first char is ']' then we have been given no key for this map
                        // Example: "book[]" when something like "book[author]" was expected
                        if (j == prefix.length() + 1) {
                            throw new UrlQueryParseException(Codes.INVALID_MAP, "No key given, map expected but looks like an list");
                        }

                        indexPart = pair[0].substring(prefix.length() + 1, j);
                        break;
                    }
                } while (true);

                switch (struc.getStructure()) {
                    case ARRAY:
                        map.put(indexPart, parseArray(level + 1, parent, keyValues, prefix + "[" + indexPart + "]", struc.getType()));
                        break;
                    case LIST:
                        map.put(indexPart, parseList(level + 1, parent, keyValues, prefix + "[" + indexPart + "]", ((ListStructure) struc).getComponentType()));
                        break;
                    case SET:
                        map.put(indexPart, parseSet(level + 1, parent, keyValues, prefix + "[" + indexPart + "]", ((SetStructure) struc).getComponentType()));
                        break;
                    case MAP:
                        map.put(indexPart, parseMap(level + 1, parent, keyValues, prefix + "[" + indexPart + "]", ((MapStructure) struc).getType()));
                        break;
                    case MODEL:
                        map.put(indexPart, ((ModelStructure) struc).getModel().parseForSubModel(parent, keyValues, prefix + "[" + indexPart + "]"));
                        break;
                    case SCALAR:
                        try {
                            map.put(indexPart, ((ScalarStructure) struc).getParser().transform(pair[1]));
                        } catch (UrlQueryParseException e) {
                            if (this.options.propagateErrors()) {
                                throw e;
                            }
                        }

                        break;
                }
            }
        }

        if (map.isEmpty() && this.options.isRequired()) {
            throw new UrlQueryException(Codes.PARAM_REQUIRED, "Param required at " + prefix);
        }

        return map;
    }

    public <T> List<T> parseList(int level, Object parent, String[][] keyValues, String prefix, java.lang.reflect.Type componentType) {
        Structure struc = this.structure.get(level);
        if (struc.getStructure() != Structure.Type.SCALAR) {
            throw new UnsupportedOperationException("List does not support non-scalars");
        }

        List<T> list = new ArrayList<>();

        for (String[] pair : keyValues) {
            if (pair[0].equals(prefix)) {
                try {
                    list.add((T) ((ScalarStructure) struc).getParser().transform(pair[1]));
                } catch (UrlQueryParseException e) {
                    if (this.options.propagateErrors()) {
                        throw e;
                    }
                }
            }
        }

        if (list.isEmpty()) {
            if (this.options.isRequired()) {
                throw new UrlQueryException(Codes.PARAM_REQUIRED, "Param required at " + prefix);
            }

            return null;
        }

        return list;
    }

    public <T> Set<T> parseSet(int level, Object parent, String[][] keyValues, String prefix, java.lang.reflect.Type componentType) {
        Structure struc = this.structure.get(level);
        if (struc.getStructure() != Structure.Type.SCALAR) {
            throw new UnsupportedOperationException("List does not support non-scalars");
        }

        Set<T> set = new HashSet<>();

        for (String[] pair : keyValues) {
            if (pair[0].equals(prefix)) {
                try {
                    set.add((T) ((ScalarStructure) struc).getParser().transform(pair[1]));
                } catch (UrlQueryParseException e) {
                    if (this.options.propagateErrors()) {
                        throw e;
                    }
                }
            }
        }

        if (set.isEmpty()) {
            if (this.options.isRequired()) {
                throw new UrlQueryException(Codes.PARAM_REQUIRED, "Param required at " + prefix);
            }

            return null;
        }

        return set;
    }

    public <T> T[] parseArray(int level, Object parent, String[][] keyValues, String prefix, Class<T> componentType) {

        Structure struc = this.structure.get(level);

        int maxIdx = -1;
        Set<Integer> indexes = new HashSet<>(keyValues.length);
        for (String[] pair : keyValues) {
            if (pair[0].startsWith(prefix + "[")) {
                String indexPart = null;

                int j = prefix.length();
                char ch;
                do {
                    if (++j == pair[0].length()) {
                        throw new UrlQueryParseException(Codes.NON_TERMINATING_BRACKET, "No terminating ] found");
                    }

                    ch = pair[0].charAt(j);
                    if (ch == ']') {
                        // If the next first char is ']' then we have been given no key for this map
                        // Example: "book[]" when something like "book[author]" was expected
                        if (j == prefix.length() + 1) {
                            throw new UrlQueryParseException(Codes.EMPTY_INDEX, "No key given, array expected but looks like an list");
                        }

                        indexPart = pair[0].substring(prefix.length() + 1, j);
                        break;
                    }
                } while (true);

                int idx;
                try {
                    idx = Integer.valueOf(indexPart);
                    if (idx < 0) {
                        throw new UrlQueryParseException(Codes.NEGATIVE_INDEX, "Negative index given");
                    }
                } catch (NumberFormatException e) {
                    throw new UrlQueryParseException(Codes.NON_NUMERIC_INDEX, "Non-numeric index given, array expected but looks like a map");
                }

                indexes.add(idx);

                // Keep track of the maximum index
                if (idx > maxIdx) {
                    maxIdx = idx;
                }
            }
        }

        if (maxIdx == -1) {
            if (this.options.isRequired()) {
                throw new UrlQueryException(Codes.PARAM_REQUIRED, "Param required at " + prefix);
            }

            return null;
        }

        T[] array = (T[]) Array.newInstance(componentType, maxIdx + 1);
        switch (struc.getStructure()) {
            case ARRAY:
                for (int idx : indexes) {
                    array[idx] = (T) parseArray(level + 1, parent, keyValues, prefix + "[" + idx + "]", struc.getType());
                }
                break;
            case SET:
                for (int idx : indexes) {
                    array[idx] = (T) parseSet(level + 1, parent, keyValues, prefix + "[" + idx + "]", struc.getType());
                }
                break;
            case LIST:
                for (int idx : indexes) {
                    array[idx] = (T) parseList(level + 1, parent, keyValues, prefix + "[" + idx + "]", struc.getType());
                }
                break;
            case MAP:
                for (int idx : indexes) {
                    array[idx] = (T) parseMap(level + 1, parent, keyValues, prefix  + "[" + idx + "]", ((MapStructure) struc).getType());
                }
                break;
            case MODEL:
                for (int idx : indexes) {
                    array[idx] = (T) ((ModelStructure) struc).getModel().parseForSubModel(parent, keyValues, prefix + "[" + idx + "]");
                }

                break;
            case SCALAR:
                for (int idx : indexes) {
                    String target = prefix + "[" + idx + "]";
                    for (String[] pair : keyValues) {
                        if (pair[0].equals(target)) {
                            try {
                                array[idx] = (T) ((ScalarStructure) struc).getParser().transform(pair[1]);
                            } catch (UrlQueryParseException e) {
                                if (this.options.propagateErrors()) {
                                    throw e;
                                }
                            }
                        }
                    }
                }

                break;
        }

        return array;
    }

    @Override
    public String toString() {
        return "CompiledParam{" +
                "field=" + field +
                ", name='" + name + '\'' +
                ", structure=" + structure +
                '}';
    }

    public <T> Object parseForSubModel(int level, T parent, String[][] keyValues, String prefix, String name) {
        Structure struc = this.structure.get(level);
        switch (struc.getStructure()) {
            case ARRAY:
                return parseArray(level + 1, parent, keyValues, prefix+"["+name+"]", struc.getType());
            case LIST:
                return parseList(level + 1, parent, keyValues, prefix+"["+name+"]", ((ListStructure) struc).getComponentType());
            case SET:
                return parseSet(level + 1, parent, keyValues, prefix+"["+name+"]", ((ListStructure) struc).getComponentType());
            case MAP:
                return parseMap(level + 1, parent, keyValues, prefix+"["+name+"]", ((MapStructure) struc).getType());
            case MODEL:
                return ((ModelStructure) struc).getModel().parseForSubModel(parent, keyValues, prefix + "["+name+"]");
            case SCALAR:
                for (String[] pair : keyValues) {
                    if (pair[0].equals(prefix+"["+name+"]")) {
                        try {
                            return ((ScalarStructure) struc).getParser().transform(pair[1]);
                        } catch (UrlQueryParseException e) {
                            if (this.options.propagateErrors()) {
                                throw e;
                            }

                            break;
                        }
                    }
                }

                if (this.options.isRequired()) {
                    throw new UrlQueryException(Codes.PARAM_REQUIRED, "Param required at " + prefix);
                }

                break;
        }

        return null;
    }

}
