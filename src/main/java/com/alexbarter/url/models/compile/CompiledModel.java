/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.url.models.compile;

import javax.annotation.Nullable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.*;

public final class CompiledModel<T> {

    @Nullable
    private Constructor<T> emptyConstructor;
    @Nullable
    private Constructor<T> parentConstructor;
    private List<CompiledParam> compiledParams;

    public CompiledModel(Constructor<T> emptyConstructor, Constructor<T> parentConstructor, List<CompiledParam> compiledParams) {
        this.emptyConstructor = emptyConstructor;
        this.parentConstructor = parentConstructor;
        this.compiledParams = compiledParams;
    }

    public T parse(String queryStr) {
        return this.parse(queryStr, Charset.forName("UTF-8"));
    }

    public T parse(String queryStr, Charset charSet) {
        T model = null;
        try {
            model = parseStrInternal(queryStr, charSet);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return model;
    }

    private T parseStrInternal(String queryStr, Charset charSet) throws IllegalAccessException, InvocationTargetException, InstantiationException, UnsupportedEncodingException {
        T model = this.emptyConstructor.newInstance();

        String[] pairs = queryStr.split("&");

        String[][] keyValues = new String[pairs.length][2];
        for (int i = 0; i < pairs.length; i++) {
            keyValues[i] = pairs[i].split("=", 2);
            keyValues[i][0] = URLDecoder.decode(keyValues[i][0], charSet.name());
            keyValues[i][1] = URLDecoder.decode(keyValues[i][1], charSet.name());
        }

        for (CompiledParam cp : this.compiledParams) {
            Field field = cp.getField();
            System.out.println("Parsing field: "  + field.getName());
            Object obj = cp.parse(model, keyValues);
            //System.out.println(obj != null && obj.getClass().isArray() ? Arrays.toString((Object[]) obj) : Objects.toString(obj));
            if (obj != null) {
                field.set(model, obj);
            }
            System.out.println("Set field: " + field.getName() +" -> " + (obj != null && obj.getClass().isArray() ? Arrays.toString((Object[]) obj) : Objects.toString(obj)));
        }

        return model;
    }

    public T parseForSubModel(Object parent, String[][] keyValues, String prefix) {
        try {
            T model = parseForSubModelInternal(parent, keyValues, prefix);
            return model;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        return null;
    }

    private T parseForSubModelInternal(Object parent, String[][] keyValues, String prefix) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        T model = this.parentConstructor != null ? this.parentConstructor.newInstance(parent) : this.emptyConstructor.newInstance();

        for (CompiledParam cp : this.compiledParams) {
            Field field = cp.getField();
            Object obj = cp.parseForSubModel(0, model, keyValues, prefix, cp.getName());
            System.out.println(obj != null && obj.getClass().isArray() ? Arrays.toString((Object[]) obj) : Objects.toString(obj));
            if (obj != null) {
                field.set(model, obj);
            }
        }

        return model;
    }

    @Override
    public String toString() {
        return "CompiledParamModel{" +
                "compiledParams=" + compiledParams +
                '}';
    }
}
