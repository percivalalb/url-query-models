/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.url.models.impl;

import com.alexbarter.url.models.definition.Blacklist;
import com.alexbarter.url.models.definition.Model;

import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import static com.alexbarter.url.models.definition.Required.*;

@Model(required=YES)
public class DataTableModel implements Supplier<DataTableModel> {

    public int draw;
    public int start;
    public int length;
    public OrderSubModel[] order;
    public ColumnSubModel[] columns;
    public SearchSubModel<DataTableModel> search;

    @Override
    public DataTableModel get() {
        return this;
    }

    @Override
    public String toString() {
        return "DataTableModel{" +
                "draw=" + draw +
                ", start=" + start +
                ", length=" + length +
                ", order=" + Arrays.toString(order) +
                ", columns=" + Arrays.toString(columns) +
                ", search=" + search +
                '}';
    }

    public String getSQLOrder(String[] columnNames) {
        StringJoiner whereJoiner = new StringJoiner(",").setEmptyValue("");
        for (int i = 0; i < this.order.length; i++) {
            if (!DataTableModel.this.columns[this.order[i].column].orderable) {
                continue;
            }

            String sql = DataTableModel.this.order[i].toSql(columnNames);

            if (sql != null) {
                 whereJoiner.add(sql);
            }
        }
        return whereJoiner.toString();
    }

    public Object[] getSQLSearch(String[] columnNames) {
        StringJoiner whereJoiner = new StringJoiner(" AND ", "(", ")").setEmptyValue("");
        List<String> bindParams = new ArrayList<>();
        for (int i = 0; i < columnNames.length; i++) {
            if (!this.columns[i].searchable) {
                continue;
            }

            String[] sql = this.columns[i].search.toSql(columnNames[i]);

            if (sql != null) {
                whereJoiner.add(sql[0]);
                bindParams.add(sql[1]);
            }
        }
        return new Object[] {whereJoiner.toString(), bindParams};
    }

    @Model
    public class OrderSubModel {
        public Integer column;
        public String dir;

        public String toSql(String[] columnNames) {
            return columnNames[this.column] + " " + (this.dir.equals("asc") ? "ASC" : "DESC");
        }
    }

    @Model
    public class ColumnSubModel implements Supplier<DataTableModel> {

        public String name;
        public Boolean orderable;
        public Boolean searchable;
        public SearchSubModel<ColumnSubModel> search;

        @Override
        public String toString() {
            return "ColumnSubModel{" +
                    "name='" + name + '\'' +
                    ", orderable=" + orderable +
                    ", searchable=" + searchable +
                    ", search=" + search +
                    '}';
        }

        @Override
        public DataTableModel get() {
            return DataTableModel.this;
        }
    }

    @Model
    public static class SearchSubModel<T extends Supplier<DataTableModel>> {
        public String value;
        public Boolean regex;
        @Blacklist
        private Map<Integer, Pattern> patterns = new HashMap<>();
        @Blacklist
        private T parent;

        public SearchSubModel(T parent) {
            this.parent = parent;
        }

        @Override
        public String toString() {
            return "SearchSubModel{" +
                    "value='" + value + '\'' +
                    ", regex=" + regex +
                    '}';
        }

        /**
         * Returns a case insensitive pattern which matches unicode for search term.
         *
         * @return The search pattern
         */
        public Pattern getPattern() {
            return this.getPattern(Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
        }

        public Pattern getPattern(int flag) {
            return this.patterns.computeIfAbsent(flag, this::compilePattern);
        }

        private Pattern compilePattern(int flag) {
            if (!this.regex) {
                return Pattern.compile("\\Q" + this.value + "\\E", flag);
            }

            return Pattern.compile(this.value, flag);
        }

        /**
         * Returns true if the search term is contained in the given data.
         * If regex=false the term must match exactly, if regex=true
         * then the term matches as regex.
         *
         * @param data The string to run the pattern over
         * @return If the search term is contained in the data
         */
        public boolean termIn(String data) {
            return this.getPattern().matcher(data).find();
        }

        /**
         * Filters out the elements of the list which do not contain the search
         * term.
         *
         * @param list The list to filter
         * @return The filtered list, all elements must contain the search term
         */
        public List<String> filter(List<String> list) {
            List<String> result = new ArrayList<>();
            for (String data : list) {
                if (this.termIn(data)) {
                    result.add(data);
                }
            }

            return result;
        }

        public Object[] getSQLSearch(String[] columnNames) {
            StringJoiner whereJoiner = new StringJoiner(" OR ", "(", ")").setEmptyValue("");
            List<String> bindParams = new ArrayList<>();
            for (int i = 0; i < columnNames.length; i++) {
                if (!this.parent.get().columns[i].searchable) {
                    continue;
                }

                String[] sql = this.toSql(columnNames[i]);

                if (sql != null) {
                    whereJoiner.add(sql[0]);
                    bindParams.add(sql[1]);
                }
            }
            return new Object[] {whereJoiner.toString(), bindParams};
        }

        public String[] toSql(String columnName) {
            if (this.value == null || this.value.isEmpty()) {
                return null;
            }

            if (!this.regex) {
                // TODO escape % & _ in this.value
                return new String[] {columnName + " LIKE CONCAT('%',?,'%')", this.value};
            }

            String regex = this.value;
//            if (this.value.endsWith("\\")) {
//                int slashesAtEnd = 1;
//                for (int i = regex.length() - 2; i >= 0; i--) {
//                    if (regex.charAt(i) != '\\') {
//                        break;
//                    }
//
//                    slashesAtEnd++;
//                }
//
//                System.out.println("Slashes " + slashesAtEnd);
//
//                if (slashesAtEnd % 2 == 1) {
//                    regex = this.value + '\\';
//                }
//            }

            Pattern.compile(regex);
            return new String[] {columnName + " REGEXP ?", regex};
        }
    }
}
