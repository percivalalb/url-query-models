/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.url.models;

import com.alexbarter.url.models.compile.*;
import com.alexbarter.url.models.compile.exception.Codes;
import com.alexbarter.url.models.compile.exception.UrlQueryCompileException;
import com.alexbarter.url.models.compile.structure.*;
import com.alexbarter.url.models.definition.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.*;
import java.util.*;

public class URLQueryModel {

    private static final Map<Class, CompiledModel> compiledSubModels = new HashMap<>();

    public static <T> CompiledModel<T> compile(Class<T> template) {
        return compile(template, null, null, null);
    }

    public static <T> CompiledModel<T> compile(Class<T> template, Class<T> parentClass, Required parentRequired, Required parentErrors) {
        CompiledModel<T> compiledParamModel = compiledSubModels.getOrDefault(template, null);
        if (compiledParamModel != null) {
            return compiledParamModel;
        }

        if (template.getEnclosingMethod() != null) {
            throw new UrlQueryCompileException(Codes.INVALID_ENCLOSING, "Local or anonymous classes within a method is not supported");
        }

        if (template.getEnclosingConstructor() != null) {
            throw new UrlQueryCompileException(Codes.INVALID_ENCLOSING, "Local or anonymous classes within a constructor is not supported");
        }

        // Static classes only exist as nested classes so, static => nested <=> has enclosing class
        boolean isStatic = Modifier.isStatic(template.getModifiers());
        Class enclosingClass = template.getEnclosingClass();

        Constructor emptyConstructor = null;
        Constructor parentConstructor = null;

        if (!isStatic) {
            try {
                if (enclosingClass == null) {
                    emptyConstructor = template.getConstructor();
                } else {
                    parentConstructor = template.getConstructor(enclosingClass);
                }
            } catch (NoSuchMethodException e) {
                throw new UrlQueryCompileException(Codes.INVALID_CONSTRUCTOR, "Must be an empty constructor in inner class");
            }
        } else {
            try {
                emptyConstructor = template.getConstructor();
            } catch (NoSuchMethodException e) {
                System.out.println(Arrays.toString(template.getDeclaredConstructors()));
            }

            // parentClass is the class which contains the field of this type
            if (parentClass != null) {
                for (Constructor cons : template.getConstructors()) {
                    if (cons.getParameterCount() != 1) {
                        continue;
                    }

                    // TODO: Support multiple single argument constructors
                    if (parentConstructor != null) {
                        throw new UrlQueryCompileException(Codes.INVALID_CONSTRUCTOR, String.format("Multiple constructors match the signature public %s(%s instanceof ?)", template.getName(), parentClass.getName()));
                    }

                    if (!cons.getParameterTypes()[0].isAssignableFrom(parentClass)) {
                        continue;
                    }

                    parentConstructor = cons;
                }
            }

            if (template.getConstructors().length == 0) {
                try {
                    emptyConstructor = template.getDeclaredConstructor();
                } catch (NoSuchMethodException e) {
                    System.out.println(Arrays.toString(template.getDeclaredConstructors()));
                }
            }
        }

        if (emptyConstructor == null && parentConstructor == null) {
            throw new UrlQueryCompileException(Codes.INVALID_CONSTRUCTOR, "No valid constructors found");
        }

        AutoDiscover autoDiscover = Defaults.AUTO_DISCOVER;
        Required required = Defaults.REQUIRED;
        Required propagateErrors = Defaults.PROPAGATE_ERRORS;

        Model modelOpts = template.getAnnotation(Model.class);
        if (modelOpts != null) {
            autoDiscover = modelOpts.autoDiscover();
            required = modelOpts.required();
            propagateErrors = modelOpts.errors();
        }

        required = resolveRequiredFlag(parentRequired, required);
        propagateErrors = resolveRequiredFlag(parentErrors, propagateErrors);

        Field[] fields = template.getFields();
        ArrayList<CompiledParam> compiledParams = new ArrayList<>(fields.length);

        for (Field field : fields) {
            QueryParam paramTpl = field.getAnnotation(QueryParam.class);
            if (paramTpl != null || (autoDiscover == AutoDiscover.BLACKLIST && !field.isAnnotationPresent(Blacklist.class))) {
                CompiledParam compiledParam = compileParam(template, field, required, propagateErrors, paramTpl);
                compiledParams.add(compiledParam);
            }
        }

        compiledParams.trimToSize();
        compiledParamModel = new CompiledModel<T>(
            emptyConstructor,
            parentConstructor,
            compiledParams
        );

        compiledSubModels.put(template, compiledParamModel);
        return compiledParamModel;
    }

    private static Required resolveRequiredFlag(@Nullable Required parent, Required child) {
        if (child == Required.INHERIT) {
            return parent == null || parent == Required.INHERIT ? child : parent;
        }

        return child;
    }

    public static CompiledParam compileParam(Class<?> template, @Nonnull Field field, Required parentRequired, Required parentErrors, @Nullable QueryParam paramTpl) {
        String fieldName = field.getName();

        Class<?> fieldType = field.getType();
        List<Structure> typeTree = typeTree(field, template, parentRequired, parentErrors, paramTpl);

        Required required = Defaults.REQUIRED;
        Required propagateErrors = Defaults.PROPAGATE_ERRORS;
        if (paramTpl != null) {
            required = paramTpl.required();
            propagateErrors = paramTpl.errors();
        }
        required = resolveRequiredFlag(parentRequired, required);
        propagateErrors = resolveRequiredFlag(parentErrors, propagateErrors);

        ParamOptions options = new ParamOptions(required, propagateErrors);

        return new CompiledParam(
            field,
            paramTpl == null || paramTpl.name().isEmpty() ? fieldName : paramTpl.name(),
            typeTree,
            options
        );
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static List<Structure> typeTree(@Nonnull Field field, Class parentClass, Required parentRequired, Required parentErrors, @Nullable QueryParam paramTpl) {
        List<Structure> tree = new ArrayList<>();

        //System.out.println("-------------------------------------");
        //System.out.println(field);
        typeTree(field.getType(), field.getGenericType(), tree, parentClass, parentRequired, parentErrors, paramTpl);
        //System.out.println(field + " | " + tree);
        //System.out.println(tree);
        return tree;
    }

    public static List<Structure> typeTree(Class clazz, java.lang.reflect.Type type, List<Structure> tree, Class parentClass, Required parentRequired, Required parentErrors, @Nullable final QueryParam paramTpl) {
        //System.out.println(String.format("| %s %s %s", clazz, type, parentClass));
        if (clazz.isArray()) {
            Class<?> arrCompType = clazz.getComponentType();

            tree.add(new Structure(Structure.Type.ARRAY, clazz.getComponentType()));
            if (type instanceof GenericArrayType) {
                java.lang.reflect.Type arrayType = ((GenericArrayType) type).getGenericComponentType();

                // If the array type is not a concrete type then error. E.g is T[] where <T extends String>
                if (arrayType instanceof TypeVariable) {
                    throw new UrlQueryCompileException(Codes.INVALID_GENERIC, "Array component can not be a type variable");
                }

                return typeTree(arrCompType, arrayType, tree, parentClass, parentRequired, parentErrors, paramTpl);
            }
            //tree.add(new ScalarStructure(clazz.getComponentType(), Util.getTransformer(clazz.getComponentType())));
            return typeTree(arrCompType, arrCompType, tree, parentClass, parentRequired, parentErrors, paramTpl);
        } else if (clazz == List.class) {
            System.out.println("List type:" + type.getClass());
            if (type instanceof ParameterizedType) {
                java.lang.reflect.Type[] parameterizedType = ((ParameterizedType) type).getActualTypeArguments();
                tree.add(new ListStructure(clazz, parameterizedType[0]));
                if (parameterizedType[0] instanceof ParameterizedType) {
                    return typeTree((Class) ((ParameterizedType) parameterizedType[0]).getRawType(), parameterizedType[1], tree, parentClass, parentRequired, parentErrors, paramTpl);
                }
                return typeTree((Class) parameterizedType[0], parameterizedType[0], tree, parentClass, parentRequired, parentErrors, paramTpl);
            }
            // TODO throw error
            return tree;
        } else if (clazz == Set.class) { // same as list
            System.out.println("Set type:" + type.getClass());
            if (type instanceof ParameterizedType) {
                java.lang.reflect.Type[] parameterizedType = ((ParameterizedType) type).getActualTypeArguments();
                tree.add(new SetStructure(clazz, parameterizedType[0]));
                if (parameterizedType[0] instanceof ParameterizedType) {
                    return typeTree((Class) ((ParameterizedType) parameterizedType[0]).getRawType(), parameterizedType[1], tree, parentClass, parentRequired, parentErrors, paramTpl);
                }
                return typeTree((Class) parameterizedType[0], parameterizedType[0], tree, parentClass, parentRequired, parentErrors, paramTpl);
            }
            // TODO throw error
            return tree;
        } else if (clazz == Map.class) {
            if (!(type instanceof ParameterizedType)) {
                throw new UrlQueryCompileException(Codes.INVALID_MAP, "The map must be parameterized");
            }

            java.lang.reflect.Type[] parameterizedType = ((ParameterizedType) type).getActualTypeArguments();

            if (parameterizedType.length != 2) {
                throw new UrlQueryCompileException(Codes.INVALID_MAP, "Map class must have exactly two parameterized types");
            }

            if (!isTypeSupportedMapKey(parameterizedType[0])) {
                throw new UrlQueryCompileException(Codes.INVALID_MAP, "Map key type is not supported");
            }

            if (parameterizedType[1] instanceof ParameterizedType) {
                java.lang.reflect.Type valueType = ((ParameterizedType) parameterizedType[1]).getRawType();
                tree.add(new MapStructure(clazz, (Class) parameterizedType[0], (Class) valueType));
                return typeTree((Class) valueType, parameterizedType[1], tree, parentClass, parentRequired, parentErrors, paramTpl);
            }

            tree.add(new MapStructure(clazz, (Class) parameterizedType[0], (Class) parameterizedType[1]));
            return typeTree((Class) parameterizedType[1], parameterizedType[1], tree, parentClass, parentRequired, parentErrors, paramTpl);
        } else if (clazz.getAnnotation(Model.class) != null) {
            System.out.println(clazz + " | " + type);
//            if (type instanceof ParameterizedType) {
//                Type[] parameterizedType = ((ParameterizedType) type).getActualTypeArguments();
//                tree.add(new ModelStructure(clazz.getComponentType(), compileModel(clazz.getComponentType())));
//                return tree;
//            }

            tree.add(new ModelStructure(clazz, compile(clazz, parentClass, parentRequired, parentErrors)));
            return tree;
        } else {
            IParamParser<?> parser = null;
            if (paramTpl != null) {
                Class<? extends IParamParser> parserCls = paramTpl.parser();
                if (parserCls != IParamParser.class) {
                    parser = compileParser(parserCls, paramTpl.parserParams());
                }
            }

            if (parser == null) {
                parser = Util.getTransformer(clazz);
            }

            if (parser == null) {
                throw new UrlQueryCompileException(Codes.MISSING_PARSER, "Could not find parser for type: " + type);
            }

            tree.add(new ScalarStructure(clazz, parser));
            return tree;
        }
    }

    public static <T extends IParamParser> IParamParser<T> compileParser(Class<T> clazz, String[] constructorParams) {
        try {
            Constructor<T> parserConstructor = clazz.getConstructor(toParamClass(constructorParams));
            return (IParamParser<T>) parserConstructor.newInstance((Object[]) constructorParams);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Class<?>[] toParamClass(Object[] arr) {
        Class<?>[] parameterTypes = new Class[arr.length];
        for (int i = 0; i < arr.length; i++) {
            parameterTypes[i] = arr[i].getClass();
        }
        return parameterTypes;
    }

    public static boolean isTypeSupportedMapKey(java.lang.reflect.Type clazz) {
        return clazz == String.class || clazz == Integer.class || clazz == Integer.TYPE;
    }
}
