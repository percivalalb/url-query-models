/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.url;

import com.alexbarter.url.models.definition.Model;
import com.alexbarter.url.models.definition.QueryParam;
import com.alexbarter.url.models.definition.Required;

import java.util.List;
import java.util.Set;

@Model
public class ModelWithEnum {

    public Gender gender;
    public Gender[] genderA;
    @QueryParam(errors=Required.NO)
    public List<Gender> genderL;
    public Set<Gender> genderS;

    public enum Gender {
        MALE,
        FEMALE,
        UNISEX;
    }
}
