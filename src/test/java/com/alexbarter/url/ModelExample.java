/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.url;

import com.alexbarter.url.models.definition.Model;
import com.alexbarter.url.models.definition.QueryParam;
import static com.alexbarter.url.models.definition.Required.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

@Model(required = NO)
public final class ModelExample {

    @QueryParam
    public String input;
    @QueryParam(name="int")
    public int integerVar;
    @QueryParam(name="array")
    public String[] array;
    @QueryParam(name="array2", required=INHERIT)
    public String[] array2 = {"fell", "back", "to", "value"};
    @QueryParam(required=NO)
    public LocalDate date;
    @QueryParam(required=NO)
    public LocalDateTime dateTime;
    @QueryParam(required=NO)
    public Map<String, String>[] arrayMap;
}
