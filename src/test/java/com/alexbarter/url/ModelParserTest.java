/**
 * URL Query Models - Parses URL query strings into predefined object
 * models. Handles complex data structures and transforms end values
 * into objects.
 * Copyright (C) 2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.url;

import com.alexbarter.url.models.compile.exception.Codes;
import com.alexbarter.url.models.compile.exception.UrlQueryParseException;
import com.alexbarter.url.models.impl.DataTableModel;
import com.alexbarter.url.models.URLQueryModel;
import com.alexbarter.url.models.compile.CompiledModel;
import com.alexbarter.url.models.compile.exception.UrlQueryCompileException;
import com.alexbarter.url.models.compile.structure.Structure;
import com.alexbarter.url.models.definition.Model;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class ModelParserTest {

    public static final Map<String, Integer>[] test = new HashMap[2];
    public static final Map<String, Integer>[][] test2 = new HashMap[2][2];

    @Test
    public void testTypeTree() throws NoSuchFieldException {
        Field field = ModelParserTest.class.getField("test");
        List<Structure> type = URLQueryModel.typeTree(field, null, null, null, null);
        System.out.println("|| test: " + type);

        Field field2 = ModelParserTest.class.getField("test2");
        List<Structure> type2 = URLQueryModel.typeTree(field2, null, null, null, null);
        System.out.println("|| test2: " + type2);
    }

    @Test
    public void testCompileModel() throws NoSuchFieldException {
        Field field = ModelParserTest.class.getField("test");
        Type type = ((GenericArrayType) field.getGenericType()).getGenericComponentType();
        System.out.println("Test:" + Arrays.toString(((ParameterizedType) type).getActualTypeArguments()));
        CompiledModel<ModelExample> compiledModel = URLQueryModel.compile(ModelExample.class);

        ModelExample example = compiledModel.parse("input=string&int=2&array[0]=hello&array[1]=alex&date=2021-03-01&dateTime=2021-03-01T03:30:00&arrayMap[0][string]=2&arrayMap[0][key]=test");

        assertEquals("string", example.input);
        assertEquals(2, example.integerVar);
        assertArrayEquals(new String[] {"hello", "alex"}, example.array);
        assertArrayEquals(new String[] {"fell", "back", "to", "value"}, example.array2);
        assertEquals(LocalDate.of(2021, 3, 1), example.date);
        assertEquals(LocalDateTime.of(2021, 3, 1, 3, 30, 0), example.dateTime);

        {
            CompiledModel<ModelWithArray> m = URLQueryModel.compile(ModelWithArray.class);
            assertParsingThrows(Codes.NON_NUMERIC_INDEX, () -> m.parse("array[notanumber]=v"));
            assertParsingThrows(Codes.EMPTY_INDEX, () -> m.parse("array[]=v"));
            assertParsingThrows(Codes.NON_TERMINATING_BRACKET, () -> m.parse("array[=v"));
            assertParsingThrows(Codes.NON_TERMINATING_BRACKET, () -> m.parse("array[=v"));
        }

        {
            assertCompilingThrows(Codes.INVALID_GENERIC, () -> URLQueryModel.compile(ModelWithGenericArray.class));
        }
    }

    @Test
    public void testCompileModel2() {
        CompiledModel<ModelExample> compiledModel = URLQueryModel.compile(ModelExample.class);

        ModelExample example = compiledModel.parse("input=string&int=2&array[0]=hello&array[1]=alex&array2[0]=error");

        assertEquals("string", example.input);
        assertEquals(2, example.integerVar);
        assertArrayEquals(new String[] {"hello", "alex"}, example.array);
        assertArrayEquals(new String[] {"error"}, example.array2);
    }

    @Test
    public void testCompileDataTableModel() {
        CompiledModel<DataTableModel> compiledModel = URLQueryModel.compile(DataTableModel.class);

        DataTableModel example = compiledModel.parse("draw=1&columns[0][data]=ip&columns[0][name]=&columns[0][searchable]=true&columns[0][orderable]=false&columns[0][search][value]=&columns[0][search][regex]=false&columns[1][data]=time&columns[1][name]=&columns[1][searchable]=true&columns[1][orderable]=false&columns[1][search][value]=&columns[1][search][regex]=false&columns[2][data]=path&columns[2][name]=&columns[2][searchable]=true&columns[2][orderable]=false&columns[2][search][value]=&columns[2][search][regex]=false&columns[3][data]=query&columns[3][name]=&columns[3][searchable]=true&columns[3][orderable]=false&columns[3][search][value]=&columns[3][search][regex]=false&columns[4][data]=method&columns[4][name]=&columns[4][searchable]=true&columns[4][orderable]=true&columns[4][search][value]=&columns[4][search][regex]=false&columns[5][data]=headers&columns[5][name]=&columns[5][searchable]=true&columns[5][orderable]=true&columns[5][search][value]=&columns[5][search][regex]=false&columns[6][data]=cookies&columns[6][name]=&columns[6][searchable]=true&columns[6][orderable]=true&columns[6][search][value]=&columns[6][search][regex]=false&order[0][column]=1&order[0][dir]=desc&start=0&length=10&search[value]=g&search[regex]=false&test[array][0]=one&test[array][1]=two&test[map][oneK]=oneV");

        assertEquals(1, example.draw);
        assertEquals(true, example.columns[0].searchable);
        assertEquals(false, example.columns[1].orderable);
        assertEquals("", example.columns[1].name);
        assertEquals("", example.columns[1].search.value);
        assertEquals(false, example.columns[1].search.regex);
        assertEquals(1, example.order[0].column);
        assertEquals("desc", example.order[0].dir);
        assertEquals(false, example.search.regex);
        assertEquals("g", example.search.value);

        assertFalse(example.search.termIn("jee"));
        assertTrue(example.search.termIn("g"));
        assertTrue(example.search.termIn("g'day mate"));

        assertEquals(Arrays.asList("Pig", "Dog"), example.search.filter(Arrays.asList("Sheep", "Cow", "Pig", "Horse", "Dog")));
    }

    @Test
    public void testCompileSubModel() {
        CompiledModel<SubModelExample> compiledModel = URLQueryModel.compile(SubModelExample.class);

        SubModelExample example = compiledModel.parse("sub[array][0]=one&sub[array][1]=two&sub[map][oneK][0]=oneV&sub[map2][0][oneK]=4");

        assertArrayEquals(new String[] {"one", "two"}, example.sub.array);
        //assertEquals(Map.of("oneK", new String[] {"oneV"}), example.sub.map);
        assertArrayEquals(new String[] {"oneV"}, example.sub.map.get("oneK"));
        assertEquals(4, example.sub.map2[0].get("oneK"));
    }

    @Test
    public void testCompileNestedArraysExample() {
        CompiledModel<NestedArraysExample> compiledModel = URLQueryModel.compile(NestedArraysExample.class);

        NestedArraysExample example = compiledModel.parse("input[0][0][0]=one");

        //assertArrayEquals(new String[] {"one", "two"}, example.sub.array);
        //assertEquals(Map.of("oneK", new String[] {"oneV"}), example.sub.map);
        assertEquals("one", example.input[0][0][0]);
    }

    @Test
    public void testCompileParserModel() {
        CompiledModel<CustomParserModel> compiledModel = URLQueryModel.compile(CustomParserModel.class);

        CustomParserModel example = compiledModel.parse("toLength=five");

        assertEquals(4, example.toLength);
    }

    @Test
    @Disabled
    public void testCompileParserModel2() {
        @Model
        class InvalidMapKey {
            public Map<List, String> a;
        }

        assertCompilingThrows(Codes.INVALID_MAP, () -> {
            URLQueryModel.compile(InvalidMapKey.class);
        });
    }

    @Test
    @Disabled
    public void testCompileParserModel3() {
        class IntMap<V> extends HashMap<Integer, V> {}

        @Model
        class InvalidMapKey {
            public IntMap<String> a;
        }

        assertCompilingThrows(Codes.INVALID_MAP, () -> {
            URLQueryModel.compile(InvalidMapKey.class);
        });
    }

    @Test
    public void testCompileParserList() {

        CompiledModel<ListParserModel> model = URLQueryModel.compile(ListParserModel.class);
        ListParserModel key = model.parse("a[animals]=lion&a[animals]=tiger&php[]=worst&php[]=lang");

        assertEquals(Arrays.asList("lion", "tiger"), key.a.get("animals"));
        assertEquals(Arrays.asList("worst", "lang"), key.php);
    }

    @Test
    public void testCompileParserEnum() {
        CompiledModel<ModelWithEnum> model = URLQueryModel.compile(ModelWithEnum.class);

        assertEquals(ModelWithEnum.Gender.MALE, model.parse("gender=male").gender);
        assertEquals(Arrays.asList(ModelWithEnum.Gender.MALE, ModelWithEnum.Gender.FEMALE), model.parse("genderL=male&genderL=female").genderL);
        assertArrayEquals(new ModelWithEnum.Gender[] {ModelWithEnum.Gender.MALE, ModelWithEnum.Gender.FEMALE}, model.parse("genderA[0]=male&genderA[1]=female").genderA);
        assertEquals(new HashSet(Arrays.asList(ModelWithEnum.Gender.MALE, ModelWithEnum.Gender.FEMALE)), model.parse("genderS=male&genderS=female").genderS);
        assertParsingThrows(Codes.INVALID_SCALAR, () -> model.parse("gender=unknown"));
        assertDoesNotThrow(() -> model.parse("genderL=unknown"));
    }

    @Test
    public void testCompileBigInteger() {
        CompiledModel<ModelWithBigInteger> model = URLQueryModel.compile(ModelWithBigInteger.class);

        ModelWithBigInteger example = model.parse("value=9223372036854775808"); // MAX(long) + 1

        assertEquals(new BigInteger("9223372036854775808"), example.value);
    }

    @Test
    public void testCompileBigDecimal() {
        CompiledModel<ModelWithBigDecimal> model = URLQueryModel.compile(ModelWithBigDecimal.class);

        ModelWithBigDecimal example = model.parse("value=9223372036854775808.123456789"); // MAX(long) + 1.123456789

        assertEquals(new BigDecimal("9223372036854775808.123456789"), example.value);
    }

    @Test
    public void testCompileParserSet() {

        CompiledModel<SetParserModel> model = URLQueryModel.compile(SetParserModel.class);
        {
            SetParserModel key = model.parse("a[animals]=lion&a[animals]=tiger&php[]=worst&php[]=worst&php[]=lang");

            assertEquals(new HashSet(Arrays.asList("lion", "tiger")), key.a.get("animals"));
            assertEquals(new HashSet(Arrays.asList("worst", "lang")), key.php);
        }

        {
            SetParserModel key = model.parse("a[animals]=lion");
            assertEquals(null, key.php); // eventually want an option to make it new HashSet()
        }
    }

    @Test
    @Disabled
    public void testMalformedQueryStrings() {

        CompiledModel<SimpleModel> model = URLQueryModel.compile(SimpleModel.class);
        assertDoesNotThrow(() -> model.parse("a="));
        assertDoesNotThrow(() -> model.parse("a[0="));
        assertDoesNotThrow(() -> model.parse("[="));
    }

    public UrlQueryParseException assertParsingThrows(byte expectedCode, Executable executable) {
        UrlQueryParseException e = assertThrows(UrlQueryParseException.class, executable);
        assertEquals(expectedCode, e.getCode(), "Expected code " + expectedCode + " but got: " + e.getMessage());
        return e;
    }

    public UrlQueryCompileException assertCompilingThrows(byte expectedCode, Executable executable) {
        UrlQueryCompileException e = assertThrows(UrlQueryCompileException.class, executable);
        assertEquals(expectedCode, e.getCode(), "Expected code " + expectedCode + " but got: " + e.getMessage());
        return e;
    }
}
