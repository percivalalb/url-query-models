# URL Query Models
Lightweight Java library that parses [URL query strings](https://en.wikipedia.org/wiki/Query_string)
into predefined object models. Handles complex data structures and transforms end values into objects.
A powerful yet easy to use tool.

## How to use

*Define a model*

```java
@Model
public class Person {
    public Title title; // Title is an enum with values mr, mrs ...
    public String firstName;
    public String lastName;
    public LocalDate birthday;
}
```
*Compile the model for fast parsing*
```java
private static final CompiledModel<Person> personModel = URLQueryModel.compile(Person.class);
```

*Input query strings from HTTP requests and rejoice!*
```java
Person person1 = personModel.parse("title=mr&firstName=John&lastName=Smith&birthday=1968-04-01");
Person person2 = personModel.parse("title=mrs&firstName=Emma&lastName=Smith&birthday=1972-08-31");
```

### DataTables

The power of these models can be seen when used in conjunction with [DataTables](https://datatables.net/)*
and the [server-side processing](https://datatables.net/manual/server-side) feature they provide.

Continuing with their server-side processing [example](https://datatables.net/examples/server_side/simple.html).
We can examine a typical URL query string sent to the server, here we are searching using the term "Developer"
and the data is sorted by office then start date:

```
?draw=2&columns[0][data]=0&columns[0][name]=&columns[0][searchable]=true&columns[0][orderable]=true&columns[0][search][value]=&columns[0][search][regex]=false&columns[1][data]=1&columns[1][name]=&columns[1][searchable]=true&columns[1][orderable]=true&columns[1][search][value]=&columns[1][search][regex]=false&columns[2][data]=2&columns[2][name]=&columns[2][searchable]=true&columns[2][orderable]=true&columns[2][search][value]=&columns[2][search][regex]=false&columns[3][data]=3&columns[3][name]=&columns[3][searchable]=true&columns[3][orderable]=true&columns[3][search][value]=&columns[3][search][regex]=false&columns[4][data]=4&columns[4][name]=&columns[4][searchable]=true&columns[4][orderable]=true&columns[4][search][value]=&columns[4][search][regex]=false&columns[5][data]=5&columns[5][name]=&columns[5][searchable]=true&columns[5][orderable]=true&columns[5][search][value]=&columns[5][search][regex]=false&order[0][column]=3&order[0][dir]=asc&order[1][column]=4&order[1][dir]=asc&start=0&length=10&search[value]=Developer&search[regex]=false
```

Quite *unwieldy*! It has a number of different structures, array of maps, maps of maps & different scalars,
which all need parsing, validating and then utilised.

```java
CompiledModel<DataTableModel> personModel = URLQueryModel.compile(DataTableModel.class);

DataTableModel datatableModel = personModel.parse("draw=2&columns[0][data]=0...");
```
#### Access data directly
```java
datatableModel.draw; // == 2 (Integer)
datatableModel.search.regex; // == false (Boolean)
datatableModel.search.value; // == "Developer" (String)
```

It's that simple! Another perk is that data is validated out the box and is designed to not be vulnerable to malicious
input (like SQL injection).

#### Helper methods

If using an SQL based database processor then define DataTable column index to database column alias:
```java
String[] DATATABLE_IDX_TO_DB_COLUMNS = {"first_name", "last_name", "position", "office", "start_date", "salary"};
```

`getSQLSearch` generates a logical statement to search all the searchable fields using the global search term, as well
as search terms applied to each column.
```java
Object[] searchParts = datatableModel.search.getSQLSearch(DATATABLE_IDX_TO_DB_COLUMNS);
searchParts[0]; // "(first_name LIKE CONCAT('%',?,'%') OR ... OR salary LIKE CONCAT('%',?,'%'))"
searchParts[1]; // {"Developer", "Developer", ..., "Developer"}

Object[] searchParts = datatableModel.getSQLSearch(DATATABLE_IDX_TO_DB_COLUMNS);
searchParts[0]; // "" - A conjunction of filters for each row
searchParts[1]; // {} - The associated bind params
```

`getSQLOrder` generates the order statement.
```java
String orderPart = datatableModel.getSQLOrder(DATATABLE_IDX_TO_DB_COLUMNS);
orderPart; // "office ASC,  start_date ASC"
```

Other processing on arrays and lists is supported via:

`termIn(String)`, `filter(List<String>)`, `getPattern()`

*<small>DataTables 1.10+ is supported. DataTables 1.9- is not currently due to using a
different set of parameters to send and receive from the server</small>

### Configuration

When defining a model the `@Model` annotation is used. This provides access to a number of configuration
options for the model
#### `autoDiscover` (Default `BLACKLIST`)
- `BLACKLIST` fields within the annotated class are automatically used as possible query params, the `@Blacklist` annotation must be applied to any fields which are not desired to be discovered.
- `WHITELIST` only fields which are explicitly annotated by `@QueryParam` will be discovered.

#### `required` (Default `true`)

Discoverable fields can be annotation using `@QueryParam` which gives the option to configure finer details.

#### `name` (Default `$FIELD_NAME`)

#### `required` (Default: `true`)

#### `parser` (Default: Respects type)

## How to get

*url-query-models* is published to Maven Central so can be easily included in most build tools. Gradle for instance...

#### build.gradle:

```groovy
repositories {
    mavenCentral()
}
dependencies {
    implementation 'com.alexbarter.url-query-models:0.1.*'
}
```

#### Prerequisites

- Java 8 or newer

## Development

During the 0.x versions there is no API contract, so class location & names, naming of methods might change between
builds without warning.

If you would like to contribute to the project feel free to make a merge request! Or if you find any bugs then please
make an issue, and I will fix asap. Since this is a side project, and have a full time job, development might be rather
random, as some days I might feel like working on it a lot, and other days not...
